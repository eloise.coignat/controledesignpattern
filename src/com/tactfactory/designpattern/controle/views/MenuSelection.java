package com.tactfactory.designpattern.controle.views;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.tactfactory.designpattern.controle.entities.Meal;
import com.tactfactory.designpattern.controle.abstractfactory.*;

public class MenuSelection extends JFrame {

  private Home home;
  private Meal meal;
  private MenuFactory menuFactory;

  private JButton bestOf = new JButton("BestOf");
  private JButton maxiBestOf = new JButton("MaxiBestOf");
  private JButton menuGolden = new JButton("Golden");
  private JButton happyMeal = new JButton("HappyMeal");

  private JButton burger1 = new JButton("Burger1");
  private JButton burger2 = new JButton("Burger2");

  private JButton drink1 = new JButton("Drink1");
  private JButton drink2 = new JButton("Drink2");

  private JButton fries = new JButton("Fries");
  private JButton potatoes = new JButton("Potatoes");

  private JButton validate = new JButton("Valider");

  public MenuSelection() {
    this.setTitle("Menu");
    this.setSize(400, 350);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    bindActions();
    addButtons();

    this.setVisible(true);
  }

  private void addButtons() {
    JPanel container = new JPanel();
    container.setLayout(new GridLayout(3, 1));

    JPanel containerType = new JPanel();
    containerType.setLayout(new GridLayout(4, 2));
    containerType.add(bestOf);
    containerType.add(maxiBestOf);
    containerType.add(menuGolden);
    containerType.add(happyMeal);
    container.add(containerType);

    JPanel containerBurger = new JPanel();
    containerBurger.setLayout(new GridLayout(2, 4));
    containerBurger.add(burger1);
    containerBurger.add(burger2);
    container.add(containerBurger);

    JPanel containerDrink = new JPanel();
    containerDrink.setLayout(new GridLayout(2, 4));
    containerDrink.add(drink1);
    containerDrink.add(drink2);
    container.add(containerDrink);

    JPanel containerAccompaniment = new JPanel();
    containerAccompaniment.setLayout(new GridLayout(2, 4));
    containerAccompaniment.add(fries);
    containerAccompaniment.add(potatoes);
    container.add(containerAccompaniment);

    container.add(validate);
    this.setContentPane(container);
  }

  private void bindActions() {

	  // Actions code here
	    bestOf.addActionListener(new ActionListener() {

	      @Override
	      public void actionPerformed(ActionEvent e) {
	        menuFactory = meal.createMenu("MenuBestOf");
	      }
	    });
	    
	    maxiBestOf.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	menuFactory = meal.createMenu("MenuMaxiBestOf");
	        }
	      });
	    
	    menuGolden.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	menuFactory = meal.createMenu("MenuGolden");
	        }
	      });
	    
	    happyMeal.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	menuFactory = meal.createMenu("MenuHappyMeal");
	        	menuFactory.setGame("Jeu pour les enfants");
	        }
	      });
	    
	    burger1.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setBurger("Burger1");
	        	}     
	        }
	      });
	    
	    burger2.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setBurger("Burger2");
	        	}	        	
	        }
	      });
	    
	    drink1.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setDrink("Drink1");
	        	}     
	        }
	      });
	    
	    drink2.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setDrink("Drink2");
	        	}     	
	        }
	      });
	    
	    fries.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setAccompagnement("Fries");
	        	}
	        }
	      });

	    potatoes.addActionListener(new ActionListener() {

	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (menuFactory != null) {
	        		menuFactory.setAccompagnement("Potatoes");
	        	}	        	
	        }
	      });
	    
	    validate.addActionListener(new ActionListener() {

	      @Override
	      public void actionPerformed(ActionEvent e) {    	  
	    	  if (menuFactory != null
	    			  && menuFactory.getBurger() != null
	    			  && menuFactory.getDrink() != null
	    			  && menuFactory.getAccompagnement() != null) {
	              home.getMeal().addItem(menuFactory);
	              home.setVisible(true);
	              MenuSelection.this.dispose();
	    	  } 
	      }
	    });
  }

  public void setHome(Home home) {
    this.home = home;
    meal = home.getMeal();
    home.setVisible(false);
  }
}
