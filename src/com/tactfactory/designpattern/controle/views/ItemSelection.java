package com.tactfactory.designpattern.controle.views;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.tactfactory.designpattern.controle.builder.MealBuilder;
import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Meal;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Big;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Medium;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Small;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Big;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Medium;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Small;
import com.tactfactory.designpattern.controle.entities.food.Burger1;
import com.tactfactory.designpattern.controle.entities.food.Burger2;
import com.tactfactory.designpattern.controle.entities.food.FriesBig;
import com.tactfactory.designpattern.controle.entities.food.FriesMedium;
import com.tactfactory.designpattern.controle.entities.food.FriesSmall;
import com.tactfactory.designpattern.controle.entities.food.PotatoesBig;
import com.tactfactory.designpattern.controle.entities.food.PotatoesMedium;
import com.tactfactory.designpattern.controle.entities.food.PotatoesSmall;

public class ItemSelection extends JFrame {

  private Home home;
  private Meal meal;
  private MealBuilder mealBuilder  = new MealBuilder();

  private JButton burger1 = new JButton("Burger1");
  private JButton burger2 = new JButton("Burger2");

  private JButton drink1Small = new JButton("Drink1Small");
  private JButton drink1Medium = new JButton("Drink1Medium");
  private JButton drink1Big = new JButton("Drink1Big");

  private JButton drink2Small = new JButton("Drink2Small");
  private JButton drink2Medium = new JButton("Drink2Medium");
  private JButton drink2Big = new JButton("Drink2Big");

  private JButton friesSmall = new JButton("FriesSmall");
  private JButton friesMedium = new JButton("FriesMedium");
  private JButton friesBig = new JButton("FriesBig");

  private JButton potatoesSmall = new JButton("PotatoesSmall");
  private JButton potatoesMedium = new JButton("PotatoesMedium");
  private JButton potatoesBig = new JButton("PotatoesBig");

  private JButton validate = new JButton("Valider");

  public ItemSelection() {
    this.setTitle("Items");
    this.setSize(400, 200);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    bindActions();
    addButtons();

    this.setVisible(true);
  }

  private void addButtons() {
    JPanel container = new JPanel();
    container.setLayout(new GridLayout(3, 1));

    JPanel containerBurger = new JPanel();
    containerBurger.setLayout(new GridLayout(2, 4));
    containerBurger.add(burger1);
    containerBurger.add(burger2);
    container.add(containerBurger);

    JPanel containerDrink = new JPanel();
    containerDrink.setLayout(new GridLayout(2, 3));
    containerDrink.add(drink1Small);
    containerDrink.add(drink1Medium);
    containerDrink.add(drink1Big);
    containerDrink.add(drink2Small);
    containerDrink.add(drink2Medium);
    containerDrink.add(drink2Big);
    container.add(containerDrink);

    JPanel containerAccompaniment = new JPanel();
    containerAccompaniment.setLayout(new GridLayout(2, 3));
    containerAccompaniment.add(friesSmall);
    containerAccompaniment.add(friesMedium);
    containerAccompaniment.add(friesBig);
    containerAccompaniment.add(potatoesSmall);
    containerAccompaniment.add(potatoesMedium);
    containerAccompaniment.add(potatoesBig);
    container.add(containerAccompaniment);

    container.add(validate);
    this.setContentPane(container);
  }

  private void bindActions() {

	// Actions code here
	burger1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Burger1());
		}
	});
		  
	burger2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Burger2());
		}
	});
		  
	drink1Big.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink1Big());
		}
	});
		  
	drink1Medium.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink1Medium());
		}
	});
		  
	drink1Small.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink1Small());
		}
	});
		  
	drink2Big.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink2Big());
		}
	});
		  
	drink2Medium.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink2Medium());
		}
	});
		  
	drink2Small.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new Drink2Small());
		}
	});
		  
	friesBig.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new FriesBig());
		}
	});
		  
	friesMedium.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new FriesMedium());
		}
	});
		  
	friesSmall.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new FriesSmall());
		}
	});
		  
	potatoesBig.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new PotatoesBig());
		}
	});
		  
	potatoesMedium.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    mealBuilder.addItem(new PotatoesMedium());
		}
	});
		  
	potatoesSmall.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			mealBuilder.addItem(new PotatoesSmall());
		}
	});

    validate.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
    	  meal = mealBuilder.build();
    	  for (Item item : meal.getItems()) {
    		  home.getMeal().addItem(item);
    	  }
    	  home.setVisible(true);
    	  ItemSelection.this.dispose();
      }
    });
  }

  public void setHome(Home home) {
    this.home = home;
    meal = home.getMeal();
    home.setVisible(false);
  }
}
