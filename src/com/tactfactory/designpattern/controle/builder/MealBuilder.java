package com.tactfactory.designpattern.controle.builder;

import java.util.ArrayList;
import java.util.List;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Meal;

public class MealBuilder {

	private ArrayList<Item> items = new ArrayList<>();
	
	public MealBuilder addItem(Item item) {
		items.add(item);
		return this;
	}
	
	public Meal build() {
		final Meal result = new Meal();

		result.setItems(this.items);
		
		items = new ArrayList<>();
		
		return result;
	}
}
