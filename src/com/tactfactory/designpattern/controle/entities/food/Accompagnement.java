package com.tactfactory.designpattern.controle.entities.food;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Packing;

public abstract class Accompagnement implements Item {

	@Override
	public Packing packing() {
		return new BoiteCarton();
	}
	
	public abstract String size();
}
