package com.tactfactory.designpattern.controle.entities.food;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Packing;

public abstract class Burger implements Item {

	@Override
	public Packing packing() {
		return new BoiteCarton();
	}

}
