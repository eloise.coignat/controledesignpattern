package com.tactfactory.designpattern.controle.entities.food;

public class Burger1 extends Burger {
	
	public Burger1 () {
		
	}
	
	@Override
	public String name() {
		return "Burger1";
	}
	
	@Override
	public float price() {
		return 5.80F;
	}

}
