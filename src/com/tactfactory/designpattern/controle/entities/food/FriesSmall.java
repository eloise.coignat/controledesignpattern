package com.tactfactory.designpattern.controle.entities.food;

public class FriesSmall extends Accompagnement {

	public FriesSmall() {
		
	}

	@Override
	public String name() {
		return "FriesSmall";
	}
	
	@Override
	public float price() {
		return 2.50F;
	}
	
	@Override
	public String size() {
		return "Petit";
	}

}
