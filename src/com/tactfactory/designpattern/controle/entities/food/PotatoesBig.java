package com.tactfactory.designpattern.controle.entities.food;

public class PotatoesBig extends Accompagnement {

	public PotatoesBig() {
		
	}

	@Override
	public String name() {
		return "PotatoesBig";
	}
	
	@Override
	public float price() {
		return 3.80F;
	}
	
	@Override
	public String size() {
		return "Grand";
	}

}
