package com.tactfactory.designpattern.controle.entities.food;

public class PotatoesSmall extends Accompagnement {

	public PotatoesSmall() {
		
	}
	
	@Override
	public String name() {
		return "FriesSmall";
	}
	
	@Override
	public float price() {
		return 2.60F;
	}
	
	@Override
	public String size() {
		return "Petit";
	}

}
