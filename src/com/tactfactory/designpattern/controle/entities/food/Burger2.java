package com.tactfactory.designpattern.controle.entities.food;

public class Burger2 extends Burger {
	
	public Burger2 () {
		
	}
	
	@Override
	public String name() {
		return "Burger2";
	}
	
	@Override
	public float price() {
		return 6.50F;
	}

}
