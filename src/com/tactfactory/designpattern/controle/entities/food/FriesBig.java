package com.tactfactory.designpattern.controle.entities.food;

public class FriesBig extends Accompagnement {

	public FriesBig() {
		
	}

	@Override
	public String name() {
		return "FriesBig";
	}
	
	@Override
	public float price() {
		return 4.00F;
	}
	
	@Override
	public String size() {
		return "Grand";
	}
}
