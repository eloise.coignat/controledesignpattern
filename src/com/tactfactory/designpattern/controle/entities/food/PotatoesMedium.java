package com.tactfactory.designpattern.controle.entities.food;

public class PotatoesMedium extends Accompagnement {

	public PotatoesMedium() {
		
	}

	@Override
	public String name() {
		return "FriesMedium";
	}
	
	@Override
	public float price() {
		return 3.10F;
	}
	
	@Override
	public String size() {
		return "Moyen";
	}

}
