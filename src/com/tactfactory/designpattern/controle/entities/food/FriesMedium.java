package com.tactfactory.designpattern.controle.entities.food;

public class FriesMedium extends Accompagnement {

	public FriesMedium() {
		
	}

	@Override
	public String name() {
		return "FriesMedium";
	}
	
	@Override
	public float price() {
		return 3.00F;
	}
	
	@Override
	public String size() {
		return "Moyen";
	}

}
