package com.tactfactory.designpattern.controle.entities.food;

import com.tactfactory.designpattern.controle.entities.Packing;

public class BoiteCarton implements Packing {

	@Override
	public String pack() {
		return "Bo�te en carton";
	}

}
