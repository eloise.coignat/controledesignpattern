package com.tactfactory.designpattern.controle.entities.menu;

import com.tactfactory.designpattern.controle.abstractfactory.MenuFactory;
import com.tactfactory.designpattern.controle.entities.Packing;
import com.tactfactory.designpattern.controle.entities.drink.Drink;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Medium;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Small;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Medium;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Small;
import com.tactfactory.designpattern.controle.entities.food.Accompagnement;
import com.tactfactory.designpattern.controle.entities.food.Burger;
import com.tactfactory.designpattern.controle.entities.food.Burger1;
import com.tactfactory.designpattern.controle.entities.food.Burger2;
import com.tactfactory.designpattern.controle.entities.food.FriesMedium;
import com.tactfactory.designpattern.controle.entities.food.FriesSmall;
import com.tactfactory.designpattern.controle.entities.food.PotatoesMedium;
import com.tactfactory.designpattern.controle.entities.food.PotatoesSmall;

public class MenuHappyMeal extends MenuFactory {
	
	private Burger aBurger;
	private Drink aDrink;
	private Accompagnement anAccompagnement;
	private Game aGame;

	public MenuHappyMeal() {
		
	}

	@Override
	public String name() {
		return "MenuHappyMeal";
	}

	@Override
	public Packing packing() {
		return new Sachet();
	}

	@Override
	public float price() {
		return 4.00F;
	}

	@Override
	public MenuBestOf createMenuBestOf() {
		return null;
	}

	@Override
	public MenuMaxiBestOf createMenuMaxiBestOf() {
		return null;
	}

	@Override
	public Burger getBurger() {
		return aBurger;
	}

	@Override
	public void setBurger(String aBurger) {
		this.aBurger = aBurger.equalsIgnoreCase("Burger1") ? new Burger1() : new Burger2();
	}
	
	@Override
	public Accompagnement getAccompagnement() {
		return anAccompagnement;
	}

	@Override
	public void setAccompagnement(String anAccompagnement) {
		this.anAccompagnement = anAccompagnement.equals("Fries") ? new FriesSmall() : new PotatoesSmall();
	}

	@Override
	public Drink getDrink() {
		return aDrink;
	}

	@Override
	public void setDrink(String aDrink) {
		this.aDrink = aDrink.equalsIgnoreCase("Drink1") ? new Drink1Small() : new Drink2Small();
	}

	@Override
	public MenuGolden createMenuGolden() {
		return null;
	}

	@Override
	public MenuHappyMeal createMenuHappyMeal() {
		return new MenuHappyMeal();
	}

	@Override
	public Game getGame() {
		return new Game();
	}

	@Override
	public void setGame(String aGame) {
		this.aGame = new Game();
	}

}
