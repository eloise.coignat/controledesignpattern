package com.tactfactory.designpattern.controle.entities.menu;

import com.tactfactory.designpattern.controle.abstractfactory.MenuFactory;
import com.tactfactory.designpattern.controle.entities.Packing;
import com.tactfactory.designpattern.controle.entities.drink.Drink;
import com.tactfactory.designpattern.controle.entities.drink.Drink1Medium;
import com.tactfactory.designpattern.controle.entities.drink.Drink2Medium;
import com.tactfactory.designpattern.controle.entities.food.Accompagnement;
import com.tactfactory.designpattern.controle.entities.food.Burger;
import com.tactfactory.designpattern.controle.entities.food.Burger1;
import com.tactfactory.designpattern.controle.entities.food.Burger2;
import com.tactfactory.designpattern.controle.entities.food.FriesMedium;
import com.tactfactory.designpattern.controle.entities.food.PotatoesMedium;

public class MenuBestOf extends MenuFactory {
	
	private Burger aBurger;
	private Drink aDrink;
	private Accompagnement anAccompagnement;
	private Game aGame;
	
	@Override
	public String name() {
		return "MenuBestOf";
	}

	@Override
	public Packing packing() {
		return new Sachet();
	}

	@Override
	public float price() {
		return 6.50F;
	}

	@Override
	public MenuBestOf createMenuBestOf() {
		return new MenuBestOf();
	}

	@Override
	public MenuMaxiBestOf createMenuMaxiBestOf() {
		return null;
	}

	@Override
	public Burger getBurger() {
		return aBurger;
	}

	@Override
	public void setBurger(String aBurger) {
		this.aBurger = aBurger.equalsIgnoreCase("Burger1") ? new Burger1() : new Burger2();
	}
	
	@Override
	public Accompagnement getAccompagnement() {
		return anAccompagnement;
	}

	@Override
	public void setAccompagnement(String anAccompagnement) {
		this.anAccompagnement = anAccompagnement.equals("Fries") ? new FriesMedium() : new PotatoesMedium();
	}

	@Override
	public Drink getDrink() {
		return aDrink;
	}

	@Override
	public void setDrink(String aDrink) {
		this.aDrink = aDrink.equalsIgnoreCase("Drink1") ? new Drink1Medium() : new Drink2Medium();
	}

	@Override
	public MenuGolden createMenuGolden() {
		return null;
	}

	@Override
	public MenuHappyMeal createMenuHappyMeal() {
		return null;
	}

	@Override
	public Game getGame() {
		return null;
	}

	@Override
	public void setGame(String aGame) {
		this.aGame = null;
	}
}
