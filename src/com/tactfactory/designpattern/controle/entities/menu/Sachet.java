package com.tactfactory.designpattern.controle.entities.menu;

import com.tactfactory.designpattern.controle.entities.Packing;

public class Sachet implements Packing {

	@Override
	public String pack() {
		return "Sachet ";
	}

}
