package com.tactfactory.designpattern.controle.entities.menu;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Packing;
import com.tactfactory.designpattern.controle.entities.food.BoiteCarton;

public class Game implements Item {

	public Game() {
		
	}

	@Override
	public String name() {
		return "Jeu pour les enfants";
	}

	@Override
	public Packing packing() {
		return new BoiteCarton();
	}

	@Override
	public float price() {
		return 0;
	}
}
