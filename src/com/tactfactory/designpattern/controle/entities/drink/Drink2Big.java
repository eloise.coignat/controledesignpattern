package com.tactfactory.designpattern.controle.entities.drink;

public class Drink2Big extends Drink {
	
	@Override
	public String name() {
		return "Drink2Big";
	}
	
	@Override
	public float price() {
		return 3.00F;
	}
	
	
}
