package com.tactfactory.designpattern.controle.entities.drink;

public class Drink2Small extends Drink {

	public Drink2Small() {
		
	}
	
	@Override
	public String name() {
		return "Drink2Small";
	}
	
	@Override
	public float price() {
		return 1.80F;
	}

}
