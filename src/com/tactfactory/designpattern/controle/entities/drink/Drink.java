package com.tactfactory.designpattern.controle.entities.drink;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.Packing;

public abstract class Drink implements Item {

	@Override
	public Packing packing() {
		return new GobeletCarton();
	}
	
	//public abstract String size();

}
