package com.tactfactory.designpattern.controle.entities.drink;

import com.tactfactory.designpattern.controle.entities.Packing;

public class GobeletCarton implements Packing {

	@Override
	public String pack() {
		return "Gobelet en carton";
	}

}
