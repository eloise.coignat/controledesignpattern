package com.tactfactory.designpattern.controle.entities.drink;

public class Drink1Medium extends Drink {

	@Override
	public String name() {
		return "Drink1Medium";
	}
	
	@Override
	public float price() {
		return 2.50F;
	}
	
	
}
