package com.tactfactory.designpattern.controle.entities.drink;

public class Drink1Big extends Drink {
	
	@Override
	public String name() {
		return "Drink1Big";
	}
	
	@Override
	public float price() {
		return 3.00F;
	}
	
	

}
