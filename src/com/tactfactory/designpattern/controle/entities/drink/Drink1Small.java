package com.tactfactory.designpattern.controle.entities.drink;

public class Drink1Small extends Drink {
	
	@Override
	public String name() {
		return "Drink1Small";
	}
	
	@Override
	public float price() {
		return 1.80F;
	}
	
	
}
