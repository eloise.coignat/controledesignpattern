package com.tactfactory.designpattern.controle.entities;

import java.util.ArrayList;
import java.util.List;

import com.tactfactory.designpattern.controle.abstractfactory.MenuFactory;
import com.tactfactory.designpattern.controle.entities.menu.*;

public class Meal {
	private List<Item> items = new ArrayList<Item>();

	public Meal addItem(Item item) {
		items.add(item);
		return this;
	}

	public Meal addItem(Item item, Integer number) {
		for (int i = 0; i < number; i++) {
			items.add(item);
		}
		return this;
	}
	
	 public List<Item> getItems(){
		return this.items;
	}
	
	public void setItems(List<Item> items) {
		this.items = items;
	}

	public float getCost() {
		float cost = 0.0f;

		for (Item item : items) {
			cost += item.price();
		}
		return cost;
	}

	public void showItems() {  
		for (Item item : items) {
			System.out.print("Item : " + item.name());
			System.out.print(", Packing : " + item.packing().pack());
			System.out.println(", Price : " + item.price());
		}
	}
	
	public MenuFactory createMenu(String menu) {
		MenuFactory menuFactory;
		
		switch (menu) {
			case "MenuBestOf":
				menuFactory = new MenuBestOf();
				break;
			case "MenuMaxiBestOf":
				menuFactory = new MenuMaxiBestOf();
				break;
			case "MenuGolden":
				menuFactory = new MenuGolden();
				break;
			case "MenuHappyMeal":
				menuFactory = new MenuHappyMeal();
				break;
			default:
				menuFactory = null;
				break;			
		}
		return menuFactory;
	}
}

