package com.tactfactory.designpattern.controle.abstractfactory;

import com.tactfactory.designpattern.controle.entities.Item;
import com.tactfactory.designpattern.controle.entities.drink.Drink;
import com.tactfactory.designpattern.controle.entities.food.Accompagnement;
import com.tactfactory.designpattern.controle.entities.food.Burger;
import com.tactfactory.designpattern.controle.entities.menu.Game;
import com.tactfactory.designpattern.controle.entities.menu.MenuBestOf;
import com.tactfactory.designpattern.controle.entities.menu.MenuGolden;
import com.tactfactory.designpattern.controle.entities.menu.MenuHappyMeal;
import com.tactfactory.designpattern.controle.entities.menu.MenuMaxiBestOf;

public abstract class MenuFactory implements Item {

	public abstract MenuBestOf createMenuBestOf();
	
	public abstract MenuMaxiBestOf createMenuMaxiBestOf();
	
	public abstract MenuGolden createMenuGolden();
	
	public abstract MenuHappyMeal createMenuHappyMeal();
	
	public abstract Burger getBurger();

	public abstract void setBurger(String aBurger);

	public abstract Drink getDrink();

	public abstract void setDrink(String aDrink);

	public abstract Accompagnement getAccompagnement();

	public abstract void setAccompagnement(String anAccompagnement);
	
	public abstract Game getGame();
	
	public abstract void setGame(String aGame);

}
